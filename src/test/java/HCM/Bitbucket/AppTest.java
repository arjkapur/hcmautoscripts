package HCM.Bitbucket;

import java.net.URI;
import java.net.URISyntaxException;
import org.openqa.selenium.WebDriver;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}
	/**
	 * Rigourous Test :-)
	 * 
	 * @throws URISyntaxException
	 */
	public void testAppHCMPass() throws URISyntaxException {
		
		
		URI serverURL = null;

		serverURL = new URI("https://admin.int.healthcaremgr.net/");
		
		//System.setProperty("webdriver.chrome.driver","./chromedriver.exe");
	    //ChromeOptions chromeOptions = new ChromeOptions();  
		//chromeOptions.addArguments("--no-sandbox");
		

		// Open a Chrome browser.
		//WebDriver driver = new ChromeDriver(chromeOptions);
		
		WebDriver driver = new HtmlUnitDriver(true);

		driver.get(serverURL.toString());
		try {
			HCMHome webElements = new HCMHome(driver);
			
			String title =driver.getTitle().toString();
			assertEquals("Healthcare Manager",title);
			
				driver.findElement(webElements.email).sendKeys("arjun.kapur@revolutionit.com.au");
				driver.findElement(webElements.password).sendKeys("Revit@123");
				driver.findElement(webElements.submitbutton).click();
			String appText = driver.findElement(webElements.LoginText).getText().toString();
			assertEquals("Your Apps",appText);
			
		} catch (Exception e) {
			System.out.println("Issue here");
			e.printStackTrace();
		} finally {
		
			driver.quit();
		}

	}
public void testAppHCMFail() throws URISyntaxException {
		
		
		
		URI serverURL = null;

		serverURL = new URI("https://mail.google.com/");
		
		//System.setProperty("webdriver.chrome.driver","./chromedriver.exe");
	    //ChromeOptions chromeOptions = new ChromeOptions();  
		//chromeOptions.addArguments("--no-sandbox");
		

		// Open a Chrome browser.
		//WebDriver driver = new ChromeDriver(chromeOptions);
		
		WebDriver driver = new HtmlUnitDriver(true);

		driver.get(serverURL.toString());
		try {
			HCMHome webElements = new HCMHome(driver);
			
			String title =driver.getTitle().toString();
			assertEquals("Healthcare Manager",title);
			
				driver.findElement(webElements.email).sendKeys("arjun.kapur@revolutionit.com.au");
				driver.findElement(webElements.password).sendKeys("Revit@123");
				driver.findElement(webElements.submitbutton).click();
				String appText = driver.findElement(webElements.LoginText).getText().toString();
				assertEquals("Your Apps",appText);
				
		} catch (Exception e) {
			System.out.println("Issue here");
			e.printStackTrace();
		} finally {
		
			driver.quit();
		}

	}
public void testAppHCMFailLogin() throws URISyntaxException {
	
	
	
	URI serverURL = null;

	serverURL = new URI("https://admin.int.healthcaremgr.net/");
	
	//System.setProperty("webdriver.chrome.driver","./chromedriver.exe");
    //ChromeOptions chromeOptions = new ChromeOptions();  
	//chromeOptions.addArguments("--no-sandbox");
	

	// Open a Chrome browser.
	//WebDriver driver = new ChromeDriver(chromeOptions);
	
	WebDriver driver = new HtmlUnitDriver(true);

	driver.get(serverURL.toString());
	try {
		HCMHome webElements = new HCMHome(driver);
		
		String title =driver.getTitle().toString();
		assertEquals("Healthcare Manager",title);
		
			driver.findElement(webElements.email).sendKeys("arjusn.kapur@revolutionit.com.au");
			driver.findElement(webElements.password).sendKeys("Revit@123");
			driver.findElement(webElements.submitbutton).click();
			String appText = driver.findElement(webElements.LoginText).getText().toString();
			assertEquals("Your Apps",appText);
			
	} catch (Exception e) {
		fail("TestCaseFailed Incorrect Credentials");
		e.printStackTrace();
	} finally {
	
		driver.quit();
	}

}
	
	

}
