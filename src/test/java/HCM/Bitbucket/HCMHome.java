package HCM.Bitbucket;

import java.net.URI;
import java.net.URISyntaxException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class HCMHome 
{	
	WebDriver driver;

	public HCMHome(WebDriver driver)
	{
		this.driver =driver;
	}

	By email = By.name("email");
	By password = By.name("password");
	By submitbutton = By.xpath("//*[@id=\"login\"]/div/div[4]/button");
	By LoginText = By.xpath("//*[@id=\"page-content-wrapper-inner\"]/div[2]/section/div/div[1]/div/span");
}
